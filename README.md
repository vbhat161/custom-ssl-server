# custom-ssl-server

A Simple SSL Server that runs on port `8888` with a self-signed certificate used for testing purposes. Copy the CA certificate(`cert.pem`) into your client to access the site.


### Instructions

1. To build the server
```
docker build -t sslserver .
```

2. To run the server

```
docker run --network host sslserver
```

Adding `--network host` shares the network stack with the host machine where you run the client
