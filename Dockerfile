FROM alpine

RUN apk add openssl ca-certificates

EXPOSE 8888

COPY key.pem /
COPY cert.pem /

CMD ["openssl", "s_server", "-key", "key.pem", "-cert", "cert.pem", "-accept", "8888", "-www"]
